# Reunião IPEA 23/02
Reunião realizada presencialmente no IPEA junto a Coordenação Geral de Gestão de Pessoas (CGPES)
A CGPES é dividida em 2 coordenações: coordenação de administração de pessoas e coordenação de capacitação e desenvolvimento de pessoas

## Resumo
Tanto a coordenação de administração de pessoas quanto a de capacitação e desenvolvimento de pessoas precisam gerar relatórios internos sob demanda para a presidência.
Para gerar os relatórios eles precisam acessar diversos sistemas e exportar os dados para o excel pois as informações estão espalhadas entre eles.

- Dados estão disponíveis nos sistemas, porém, estão espalhados
- Os dados não são públicos, a CGPES acessa os sistemas, gera os relatórios e disponibiliza via email para presidência
- Sistema mantém todas as ocorrências e histórico dos servidores, porém não tem uma forma de acesso e visualização do histórico em si, o acesso precisa ser feito registro a registro
- O extrator de dados do DW-SIAPE é utilizado pra gerar alguns relatórios, porém, os dados só são extraídos durante a noite e só ficam disponíveis no dia seguinte, assim, ele deixa de ser útil para demandas de relatórios imediatas
- Para eles, a periodicidade de atualização deve ser d-1 ou menor.
- Sobre a nossa solução. Os dados precisam ser protegidos por nível de acesso, os dados pessoais (nome, cpf, etc) só podem ficar disponíveis para a CGPES
- O processo de trabalho deles sempre acaba com uma atualização no sistema, então o sistema sempre está atualizado (exceto em casos que a atualização precisa ser feita pelo próprio servidor, como por exemplo a atualização do grau de escolaridade)

## Sistemas utilizados
- SIAPE
- SiapeNet
- eSiapeSigepe
- SIGEPE
- SIORG
  - eOrg
- FrequenciaSouGov
- SouGov (para atualizações de dados)
- PDG - pode se tornar estruturante
- Sistema para gratificação/GEC - futuro sistema
- SEI
- SisBolsas - Bolsistas (Sistema não estruturante)
- ComprasNet - Terceirizados