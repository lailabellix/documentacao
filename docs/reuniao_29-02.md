# Reunião IPEA 29/02
Reunião realizada presencialmente no IPEA junto a Coordenação-Geral de Gestão Estratégica e Orçamento (CGPGO).
A reunião contou com os responsáveis pelo planejamento orçamentário e com os responsáveis pela execução orçamentária/financeira.

## Resumo

### Planejamento orçamentário - SIOP
O ponto de partida para o orçamento é a Lei orçamentária (LOA), ela é elaborada no ano anterior e conta com todos os gastos do governo federal.
A LOA define o orçamento do órgão, chamado de dotação inicial. Após a aprovação da LOA pelo congresso a *Dotação Inicial* só pode ser alterada por alguma outra lei, e quando isso ocorre ela é atualizada e passa a ser chamada de *Dotação Atualizada*.

Depois de aprovada, a LOA é então publicada, gerando o documento de Nota de Dotação.

O planejamento orçamentário é realizado no SIOP e é separado em 2 tipos: 
1. Orçamento Qualitativo
    O orçamento qualitativo é onde são descritos os programas e as ações. Nele também é definido como as ações serão realizadas, o que será produzido, os responsáveis e os beneficiados.
2. Orçamento Quantitativo
    O orçamento quantitativo define para cada ação orçamentária os elementos de despesa, a fonte de recursos e a dotação.

Após esse processo, as dotações para cada ação estão definidas e o planejamento está finalizado podendo assim se iniciar a fase de execução.

As dotações representam o comprometimento daquele montante para uma determinada ação, assim, o órgão consegue saber quanto da dotação inicial já foi comprometida.

### Execução orçamentária - SIAFI
A execução parte das dotações definidas no planejamento e basicamente é dividida em 3 etapas: emprenho, liquidação e pagamento.


Em algum momento desses processo ocorrem os TEDs, eles são uma via de mão dupla, onde o órgão pode receber TEDs ou enviar TEDs.
No momento do recebimento/envio dos TEDs o valor do orçamento é repassado por meio de nota de crédito e é gerado um doc de disponibilidade orçamentária. ??
Esse valor precisa ser emprenhado

?? Esse valor é acrescido à dotação do órgão 


## Principais problemas identificados
- Tesouro Gerencial é utilizado para extrair os dados do SIAFI, porém, os dados só são extraídos durante a noite e só ficam disponíveis no dia seguinte
- A diretoria e presidência tem dificuldades para entender os termos técnicos dos relatórios gerados pelo sistema
- Dificuldade no controle/visualização de quanto já foi comprometido da dotação inicial
- Os valores enviados via TED geram uma NC ? e precisam ser emprenhados pelo favorecido
- Diferenças entre financeiro e orçamento
  - Não foi falado muito sobre, porém acredito que deva ser um ponto de atenção

? O doc de disponibilidade orçamentária é gerado junto com a NC?
? Qual a diferença entre nota de dotação e documento de disponibilidade orçamentária?

___
LOA - Lei orçamentária  
ND - Nota de dotação  
NC - Nota de crédito  
PF - Programação financeira 
Empenhado - Compromisso de pagamento
Liquidado - Momento da aquisição de bens ou contratação de serviços
Pago - Momento em que ocorre o pagamento
Orçamento - Planejamento
Financeiro - Execução do pagamento

## Sistemas utilizados
- SIOP (Planejamento e Orçamento)
- SIAFI (Empenho e Execução)
- Tesouro Gerencial (Extrator de dados do SIAFI)
- SCDP (Sistema de concessão de diárias e passagens)
- GEROP (Ordem de pagamento)
- GERCOMP (Ordem de compromisso)
- TransfereGov/SICONV (TEDs e convênios)
- ComprasNet (Licitações)