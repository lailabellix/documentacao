

# Relatórios

Repositório para documentação de relatórios

## Configuração

Execute `make docs` para acessar o servidor. Para construir os estáticos, execute `make build-docs`.


## Adição de novos documentos

1. Adicionar novo documento na pasta `./docs`
2. Adicionar o nome do arquivo no `mkdocs.yml`

``
nav:
  - Home: 'index.md'
``
3. testar localmente
# Visualizando 
Para visualizar os relatórios, acessar :


[https://documentacao-lappis-unb-gest-odadosipea-f80d1d8d3ab9b7e9edba1d4.gitlab.io/](https://documentacao-lappis-unb-gest-odadosipea-f80d1d8d3ab9b7e9edba1d4.gitlab.io/)


